const blocos = document.querySelector('#blocos')
const proposicao = document.querySelector('#proposicao')
const deputados = document.querySelector('#deputados')
const eventos = document.querySelector('#eventos')
const frentes = document.querySelector('#frentes')


// Proposição
function obterProposicao() {
	fetch('https://dadosabertos.camara.leg.br/api/v2/proposicoes')
		.then(res => res.json())
		.then(data => {
			console.log(data.dados['0'])
			proposicao.innerHTML = `
		<p>Proposição: ${data.dados['0'].id}</p>  
		<p>Ano: ${data.dados['0'].ano}</p> 
		<p>codTipo: ${data.dados['0'].codTipo}</p> 
       	<p>Ementa: ${data.dados['0'].ementa}</p>  
		<p>numero: ${data.dados['0'].numero}</p> 
		<p>siglaTipo: ${data.dados['0'].siglaTipo}</p> 
		
        `
		})
}

// Blocos Partidarios
function obterBlocos() {
	fetch('https://dadosabertos.camara.leg.br/api/v2/blocos')
		.then(res => res.json())
		.then(data => {
			console.log(data.dados['0'])
			console.log(data.dados['1'])
			blocos.innerHTML =
				`
		<p>Blocos: ${data.dados['0'].nome} </p>	
		<p>Blocos: ${data.dados['0'].idLegislatura} </p>	
		<p>Blocos: ${data.dados['1'].nome} </p>	
		<p>Blocos: ${data.dados['1'].idLegislatura} </p>	

		`
		})
}


// Deputados

function obterDeputados() {
	fetch('https://dadosabertos.camara.leg.br/api/v2/deputados/')
		.then(res => res.json())
		.then(data => {
		console.log(data.dados)
		deputados.innerHTML =  `
			<p>Nome: ${data.dados['0'].nome}</p>
			<p>id: ${data.dados['0'].id}</p>
			<p>Legislatura: ${data.dados['0'].idLegislatura}</p>
			<p>Partido: ${data.dados['0'].siglaPartido}</p>
			<p>UF: ${data.dados['0'].siglaUf}</p>
			
		
`		
	})
	
}

// Eventos

function obterEventos(){
	fetch('https://dadosabertos.camara.leg.br/api/v2/eventos/')
		.then(res => res.json())
		.then(data => {
			console.log(data.dados)
		eventos.innerHTML = `
		<p>Data e Hora de Início : ${data.dados['0'].dataHoraInicio}</p>
		<p>Data e Hora de Fim: ${data.dados['0'].dataHoraFim}</p>
		<p>Descricao: ${data.dados['0'].descricao}</p>
		<p>Local Camara: ${data.dados['0'].localCamara.nome}</p>
		<p>Situacao: ${data.dados['0'].situacao}</p>


`
	})
	
}


//Frentes

function obterFrentes(){
	fetch('https://dadosabertos.camara.leg.br/api/v2/frentes')
		.then(res => res.json())
		.then(data => {
			console.log(data.dados)
		frentes.innerHTML = `
		<p>Id: ${data.dados['0'].id}</p>
		<p>Título: ${data.dados['0'].titulo}</p>
		<p>Legislatura: ${data.dados['0'].idLegislatura}</p>

		

`
			
			
	})
	
}





















